<?php

// This class is a shell around the specific PDO driver class for a
// specific database.

class database
{
	var $dbh, $driver;

	/**
	 * Constructor.
	 *
	 * You must pass a $cfg array to this function which contains all the
	 * necessary fields to implement a connection to the database type you
	 * choose. At a minimum (for SQLite3):
	 * 
	 * 'dbdriv' => 'SQLite3',
	 * 'dbdata' => 'mydatabasefile'
	 * 
	 * See the PDO driver for your database to determine which fields must
	 * be defined.
	 *
	 */

	function __construct($cfg)
	{
		$dvr = strtolower($cfg['dbdriv']);
		switch ($dvr) {
		case 'sqlite':
		case 'sqlite3':
			include $cfg['grottodir'] . 'pdosqlite3.lib.php';
			$this->dbh = new pdosqlite3($cfg);
			break;
		case 'pg':
		case 'postgresql':
			include $cfg['grottodir'] . 'pbopgsql.lib.php';
			$this->dbh = new dbpostgresql($cfg);
			break;
		case 'mysql':
			include $cfg['grottodir'] . 'pdomysql.lib.php';
			$this->dbh = new dbmysql($cfg);
			break;
		}

		$this->driver = $dvr;
	}

	function status()
	{
		return $this->dbh->status();
	}

	function datadict()
	{
		$this->dbh->datadict();
	}

	static public function quote($value)
	{
		$quoted = str_replace("'", "''", $value);
		return "'" . $quoted . "'";
	}

	function prepare($table, $rec) 
	{
		return $this->dbh->prepare($table, $rec);
	}

	function begin_transaction()
	{
		$this->dbh->begin_transaction();
	}

	function begin()
	{
		$this->dbh->begin_transaction();
	}

	function query($sql)
	{
		return $this->dbh->query($sql);
	}

	function fetch()
	{
		return $this->dbh->fetch();
	}

	function fetch_all()
	{
		return $this->dbh->fetch_all();
	}

	function lastid($table)
	{
		return $this->dbh->lastid($table);
	}

	function insert($table, $record)
	{
		$this->dbh->insert($table, $record);
	}

	function update($table, $fields, $where_clause)
	{
		return $this->dbh->update($table, $fields, $where_clause);
	}

	function delete($table, $where_clause = NULL)
	{
		$this->dbh->delete($table, $where_clause);
	}

	function commit()
	{
		$this->dbh->commit();
	}

	function end()
	{
		$this->dbh->end();
	}

	function rollback()
	{
		$this->dbh->rollback();
	}

	function version()
	{
		return 2.5;
	}
};

