<?php

/**
 * instrument()
 *
 * Used in debugging. It shows the type and value of any variable.
 *
 * @param string $label What do you want to label this?
 * @param mixed $var What do you want to see?
 *
 */

function instrument($label, $var)
{
	echo $label . PHP_EOL;
	// the following makes the output of print_r() actually readable in a
	// HTML page
	echo '<pre>' . PHP_EOL;
	print_r($var);
	echo '</pre>' . PHP_EOL;
}

/**
 * Instantiate a model
 *
 * We pass the basename of the model. The model name should be
 * <basename>.mdl.php, and the class should be the same name. Note: the
 * $cfg should be present and is globalized. The $db database connection
 * object is also globalized, and it's assumed that the model needs that
 * object passed to its constructor. If not, just provide a NULL value for
 * the $db object. The use of the $db object, and the directory it is in
 * are what separate models and libraries (q.v.).
 *
 * @param string Basename of the model
 *
 * @return object Instantiated model object
 *
 */

function model($name)
{
	global $cfg, $db;

	$filename = $cfg['modeldir'] . $name . '.mdl.php';
	if (!file_exists($filename)) {
		die("Model $name doesn't exist!");
	}
	include $filename;
	$obj = new $name($db);
	return $obj;
}

/**
 * Instantiate a library.
 *
 * We pass the basename of the library file, which is assumed to be
 * <basename>.lib.php. This basename is also assumed to be the name of the
 * library class itself. Note: the $cfg variable must be present and is
 * globalized for this routine.
 *
 * @param string $name The name of the library
 * @param array|object|string Whatever the library constructor needs
 *
 * @return object The instantiated library object
 */

function library($name, $param = NULL)
{
	global $cfg;

	$filename = $cfg['libdir'] . $name . '.lib.php';
	if (!file_exists($filename)) {
		die("Library $name doesn't exist!");
	}
	include $filename;
	if (is_null($param)) {
		$obj = new $name();
	}
	else {
		$obj = new $name($param);
	}
	return $obj;
}

/**
 * Call in a grotto file.
 *
 * Include a grotto file. If it is a library, instantiate the library and
 * return the resultant object. Optionally, pass a parameter to its
 * constructor.
 *
 * @param string $type Type of file: inc or lib
 * @param string $name Basename of file (and class)
 * @param string $param Optional parameter
 *
 * @return object The object created, if any
 */

function grotto($type, $name, $param = NULL)
{
	global $cfg;

	$filename = $cfg['grottodir'] . $name . '.' . $type . '.php';
	if (!file_exists($filename)) {
		die("File $filename doesn't exist!");
	}
	include $filename;
	if ($type == 'inc') {
		return;
	}
	if (is_null($param)) {
		$obj = new $name();
	}
	else {
		$obj = new $name($param);
	}
	return $obj;
}

/**
 * Execute a view.
 *
 * This is a shorthand way to execute a view. We pass in a variety of
 * parameters to assist. Note that the $cfg, $nav and $form objects are
 * globalized so they can be used by the view file. The $view_file
 * parameter is the "basename" of the view file, whose name is assumed to
 * be <basename>.view.php.
 *
 * @param string $page_title The page title (top of the page)
 * @param array $data Any data needed by the application
 * @param string $return The URL to which we return for forms
 * @param string $view_file The basename of the view file
 * @param string $focus_field Which field to focus on, if any
 *
 */

function view($page_title, $data, $return, $view_file, $focus_field = '')
{
	global $cfg, $nav, $form, $me;

	extract($data);
	include $cfg['viewdir'] . 'head.view.php';
	include $cfg['viewdir'] . $view_file . '.view.php';
	include $cfg['viewdir'] . 'footer.view.php';
}

/**
 * Assign/check variable or divert.
 *
 * Check for the existence of a variable via the method passed ('P' for
 * POST, 'G' for GET). If found, return the variable value. Otherwise,
 * divert to the passed URL.
 *
 * @param string $varname The variable to check for
 * @param string $method 'G' = GET, 'P' = POST
 * @param string $failurl Where to go on failure
 *
 * @return string Value of variable, if present
 */

function fork($varname, $method, $failurl)
{
	if ($method == 'P') {
		$var = $_POST[$varname] ?? NULL;
	}
	elseif ($method == 'G') {
		$var = $_GET[$varname] ?? NULL;
	}
	if (is_null($var)) {
		header('Location: ' . $failurl);
		exit;
	}
	return $var;
}

/**
 * Are the required POST values present?
 *
 * This routine checks to see that the required values exist in the POST
 * array.
 *
 * @param array $post The POST array
 * @param array $indexes The POST indexes which should be present
 *
 * @return boolean TRUE if all found, FALSE otherwise
 */

function filled_out($post, $indexes)
{
	$errors = 0;
	foreach ($indexes as $key => $value) {
		if (! array_key_exists($value, $post)) {
			$errors++;
		}
		elseif (is_null($post[$value])) {
			$errors++;	
		}
	}

	return ($errors == 0) ? TRUE : FALSE;
}

function redirect($url)
{
	header("Location: $url");
	exit();
}

/**
 * Create the tables for this application.
 *
 * Generally assumes that a database exists (the connection for which is
 * passed in as a parameter), but that no tables exist. This routine looks
 * for a file called 'coldstart.<driver_type>'. Driver types are those
 * accepted by the PHP PDO class. This file should be a dump of the
 * structures of all the tables needed, in SQL format for the driver
 * involved. It then executes the SQL to create the tables.
 *
 * @param object $db Connection object
 */

function make_tables($db)
{
	global $cfg;

	// add the tables
	$cfile = 'coldstart.' . $cfg['dbdriv'];
	if (!file_exists($cfile)) {
		die("You need the file '$cfile' to start, and it's missing.");
	}
	$lines = file($cfile, FILE_IGNORE_NEW_LINES);
	foreach ($lines as $line) {
		$db->query($line);
	}
}

