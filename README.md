# Software Grotto

This repository contains a group of common include files and libraries I
use for all the web applications I develop. Rather than manage each app's
versions of these, I decided to have them all in their own repository, and
manage all the changes to them under git. So if you use a specific web
application of mine, you would have a directory structure like this:

```
mywebsite.com/
	init.php
	stuff.php
grotto/
	form.lib.php
	messages.inc.php
	...
```

If you're using one of my applications, you will have a configuration file
like `config/config.ini`. And in that configuration file, there will be two
lines like this:

```
incdir = "../grotto/"
libdir = "../grotto/"
```

You can change these lines to point to wherever you have installed the
**grotto** software.

The following is a list of the files included:

## FPDF Library

This is the `fpdf` library from Olivier Plathey. It is in its own
directory. It is a simple way to create simple PDFs.

## Parsedown Library

This is Emanuil Rusev's Parsedown library, used to parse markdown files.
See http://parsedown.org for his latest version.

## Database Library

This is two files: `database.lib.php` and `pdosqlite3.lib.php`. The first
is a generic shell which instantiates the database driver of your choice.
Currently, only SQLite3 is implemented, but more can be added. The second
file is the driver itself. This driver implements the PDO driver for
SQLite3, but with a much easier interface.

## Errors Include

This implements some functions which can be used to report errors under
PHP. It implements extensive error reporting and generates trace dumps into
an `error.log` file.

## Form Library

If you've ever tried to write forms in straight HTML, you know what a pain
it is. This library makes that job vastly easier for the programmer. All
forms handling is done with calls to the form class.

## Graph Include

I generate statistical graphs using both PHP's gd library and the `gnuplot`
utility. This simplifies creating those graphs.

## Memory Library

This makes it possible to simply store large amounts of data in a session
variable. The methods simplify inserting and deleting data from the session
variable.

## Messages Include

This include has two functions. The first allows you to insert an error (or
success) message into the session variable. The second shows that message
and any others queued, then deletes the message from the session variable.
It is a way of providing feedback to users about what they've just done,
which survive successive calls to page controllers.

## Miscellaneous Include

This includes a variety of functions I use constantly, but which don't
necessarily fit in a single category.

### instrument()

This call can be inserted anywhere, and will dump the contents of whatever
variable is passed in a readable format to the web page. I use it
extensively in debugging.

### model()

This calls up a model. Virtually all models I use need a database
connection (already built) and the application configuration, which are
provided. It includes the model code, instantiates the class, and returns
an instance of it.

### library()

This is like the `model()` function, but does not pass an instance of the
database connection, and does allow you to pass a parameter to whatever
library you are calling. As before, it includes, instantiates, and passed
back an instance of the library.

### grotto()

This is similar to the `model()` and `library()` functions, except that it
fetches and instantiates classes in the "grotto" hierarchy. Where
`library()` is used to instantiate objects in the `$cfg['libdir']`
directory, and `model()` is used to instantiate objects in the
`$cfg['modeldir']` directory, `grotto()` does this for classes or includes
in the "grotto" directory.

### view()

This function is used to call all my views. You pass a variety of
parameters to it, and it fetches the head view, the body view and the
footer view and displays them. Assuming the view is also a form, it passes
the script name which will handle that form's results. It also allows the
user to specify a "focus field" which is the first the user will land on
when the form is shown. It can be used with non-form views, and allows the
programmer to pass data to the view for display.

### fork()

This function is places at the beginning of form handlers. It checks to see
if a certain specified value is present, and whether it should arrive via a
GET or a POST. If the value is there, it is returned. Otherwise, you
specify what page controller to go.

### filled_out()

This function allows you to specify which fields *must* be filled out on a
form. It checks the POST array for those values, and returns FALSE if they
are not present. This ensures the user fills out all the fields which are
considered necessary.

### redirect()

This is just a simplification of the `header()` call in PHP.

### make_tables()

This is used for fresh installs of a given application which do not ship
with a database, which is usually necessary to run the application. This
function relies on a SQL file which, when fed to the database, will create
the tables and populate them as needed.

## Navigation Library

This simplifies the left hand navigation on my applications. It allows for
nested and flat types of menus. You simply give it the array of links, and
it displays them according to your CSS stylesheet.

## Numbers Include

For financial applications, I store dollar amounts as integers. This avoids
problems with binary math. However, I need to convert between the decimals
the user enters and the integers I store in the database, and back again.
These two functions do that.

## Pdate Library

This is a library for handling dates. It was first developed to handle
quarterly tax reports. Dates are stored primarily as Julian days, but with
other information as well. It can display or read dates in almost any
format and do simple date arithmetic easily.

## PDF_Report Library

This makes dealing with the `fpdf` library easier. Only a few calls are
needed, and there are numerous defaults to make life easier.

## Snake Include

This contains a function which allows a long list of items to be "snaked"
across a page. You pass it an array of strings, and specify the number of
columns you want, and it will return a string with your items in columns
down the page. There are two versions-- one uses table cells, and the other
just returns the stringified version for printing.

## Table2HTML Include

This is useful for debugging. You feed it an array of key/value pairs, and
it displays them in a table.

## Vimoutline Include

This roughly parses vimoutline files and formats them in a way they can be
easily displayed on a web page.

